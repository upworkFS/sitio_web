<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="jumbotron fondo-img">
<h2>About me:</h2>
<h3>Hi, my name is Felipe Acevedo.</h3>
<p>I am a software engineering student at Universidad Nacional de Colombia.
This is my portfolio page, where I will publish my ongoin work and personal projects. 
I am an enthusiastic developer with college-level experience in programming several different languages, and my strengths are Python, Java, and creating websites using PHP (Codeigniter, Laravel), JavaScript, HTML and CSS (Bootstrap).  Also I have knowledge programming Android apps using Android Studio and Java.  I am goal-oriented and motivated to learn and acquire experience, since software development is my passion and the career I am determined to pursue.

</div>