<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<div class="jumbotron">
<p>Feel free to contact me: </p>
<div class="container">
<?php echo form_open('contact/email'); ?>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Name</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="name" name="name" placeholder="First & Last Name" <?php if (isset($name)) { echo 'value = "'.$name.'"';} ?> >
            <?php if(isset($errName)){ echo "<p class='text-danger'>$errName</p>"; }?>
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="email" name="email" placeholder="example@domain.com" <?php if (isset($email)) { echo 'value = "'.$email.'"';} ?>>
            <?php if(isset($errEmail)) {echo "<p class='text-danger'>$errEmail</p>"; }?>

        </div>
    </div>
    <div class="form-group">
        <label for="message" class="col-sm-2 control-label">Message</label>
        <div class="col-sm-10">
            <textarea class="form-control" rows="4" name="message"><?php if (isset($message)) { echo $message; } ?></textarea>
            <?php if(isset($errMessage)) {echo "<p class='text-danger'>$errMessage</p>"; }?>

        </div>
    </div>
    <div class="form-group">
        <label for="human" class="col-sm-2 control-label">2 + 3 = ?</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="human" name="human" placeholder="Your Answer" >
            <?php if(isset($errHuman)) {echo "<p class='text-danger'>$errHuman</p>"; }?>

        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <input id="submit" name="submit" type="submit" value="Send" class="btn btn-primary" >
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <?php if(isset($result)){ echo $result; }?>
        </div>
    </div>
<?php echo form_close(); ?>
</div>
</div>

