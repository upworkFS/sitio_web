<!-- Page Content -->
    <div class="container">

        <!-- Portfolio Item Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">My projects in <?= $title ?>
                    <small><?= $description ?></small>
                </h1>
            </div>
        </div>
        <!-- /.row -->

        <!-- Portfolio Item Row -->
        <div class="row">

            <div class="col-md-8">
                <img class="img-responsive" src="http://placehold.it/750x500" alt="" id="project-image">
            </div>

            <div class="col-md-4">
                <h3 id="project-title">Move your mouse over any of the listed projects to view their description</h3>
                <p id="project-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>
                <h3 id="project-details-title">Project Details</h3>
                <ul id="project-details">
                
                </ul>
            </div>

        </div>
        <!-- /.row -->

        <!-- Related Projects Row -->


   

        <div class="row">

            <div class="col-lg-12">
                <h3 class="page-header">Projects</h3>
            </div>

            <?php 
            $contador = 0;
            foreach($proyects as $proyect):
            ?>
                <div class="col-sm-3 col-xs-6 galeria" id="project<?php echo $contador ?>" onclick="show('<?php echo $contador; ?>')">
                    <a href="#">
                    <img class="img-responsive portfolio-item" src="http://placehold.it/500x300" alt="<?php echo $proyect[0]?>">
                    <?php echo $proyect[0];
                        $characteristics = array();
                        array_push($characteristics, $proyect[2]);
                    ?>
                    </a>

                    
                </div>
            <?php 
            $contador += 1;
            //if ($contador == 4)
            endforeach;
            ?>

            

        </div>
        <!-- /.row -->

        <script type="text/javascript">
        
        var project_title = document.getElementById("project-title");
        var project_description = document.getElementById("project-description");
        var project_details_title = document.getElementById("project-details-title");
        var project_details = document.getElementById("project-details");
        var project_image = document.getElementById("project-image");
        project_title.style.display = 'none';
        project_description.style.display = 'none';
        project_details_title.style.display = 'none';
        project_details.style.display = 'none';
        project_image.style.display = 'none';

        var projects = <?php echo json_encode($proyects); ?>;
         
        
        
        var chars = <?php echo json_encode($characteristics); ?>;

        function show(element){
            project_title.innerHTML = projects[element][0];
            project_description.innerHTML = projects[element][1];

            project_title.style.display = 'inherit';
            project_description.style.display = 'inherit';
            project_details_title.style.display = 'inherit';
            project_details.style.display = 'inherit';
            project_image.style.display = 'inherit';       

            while (project_details.firstChild) {
                project_details.removeChild(project_details.firstChild);
            }

            for (i = 0; i < projects[element][2].length; i++)
            {
                var new_element = document.createElement("li");
                var text = document.createTextNode(projects[element][2][i]);
                new_element.appendChild(text);
                project_details.appendChild(new_element);

            }  

           }





        //adescripcion.onmouseover=function(){myScript};
        </script>