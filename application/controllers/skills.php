<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skills extends CI_Controller {

	/**

	 */
	public function python()
	{
		$data['title'] = "Python";
		$data['description'] = "Scripts & desktop apps";
		$proyects = array(array('Sudoku Solver', 'Simple Sudoku Solver', array('Graphical user interface', 'Friendly', 'Scalable')), 
						array('Perrito', 'Gatico', array('Genial', '10/10 would program again')));
		$data['proyects'] = $proyects;
		$this->load->vars($data);
		$this->load->view('header');
		$this->load->view('skill');
		$this->load->view('footer');
	}

}
