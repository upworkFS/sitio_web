<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['title'] = "Contact";
		$this->load->vars($data);
		$this->load->view('header');
		$this->load->view('contact');
		$this->load->view('footer');
	}

	public function email()
	{
        $name = $_POST['name'];
        $email = $_POST['email'];
        $message = $_POST['message'];
        $human = intval($_POST['human']);
        $from = 'Demo Contact Form'; 
        $to = 'felipeacevedo276@gmail.com'; 
        $subject = 'Message from Portfolio - Contact';
        
        $body = "From: $name\r\n E-Mail: $email\r\n Message:\r\n $message";
        $body = wordwrap($body,70, "\r\n");

 
        
        // Check if name has been entered
        if (!$_POST['name']) {
            $data['errName'] = 'Please enter your name';
        }
        $data['name'] = $name;
        
        // Check if email has been entered and is valid
        if (!$_POST['email'] || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $data['errEmail'] = 'Please enter a valid email address';
        }

        $data['email'] = $email;
        
        //Check if message has been entered
        if (!$_POST['message']) {
            $data['errMessage'] = 'Please enter your message';
        }

        $data['message'] = $message;
        //Check if simple anti-bot test is correct
        if ($human !== 5) {
            $data['errHuman'] = 'Your anti-spam is incorrect';
        }
     
        // If there are no errors, send the email
        if (!isset($data['errName']) && !isset($data['errEmail']) && !isset($data['errMessage']) && !isset($data['errHuman'])) {
            if (mail($to, $subject, $body)) {

            $this->load->library('email');

            $config['protocol']    = 'smtp';

            $config['smtp_host']    = 'ssl://smtp.gmail.com';

            $config['smtp_port']    = '465';

            $config['smtp_timeout'] = '7';

            $config['smtp_user']    = 'felipeacevedo276@gmail.com';

            $config['smtp_pass']    = 'FSav2103';

            $config['charset']    = 'utf-8';

            $config['newline']    = "\r\n";

            $config['mailtype'] = 'text'; // or html

            $config['validation'] = TRUE; // bool whether to validate email or not      

            $this->email->initialize($config);


            $this->email->from($email, $name);
            $this->email->to('felipeacevedo276@gmail.com'); 


            $this->email->subject($subject);

            $this->email->message($body);  

            $this->email->send();

            echo $this->email->print_debugger();

                $data['result'] = '<div class="alert alert-success">Thank You! I will be in touch</div>';
            } 
            else {
                $data['result']='<div class="alert alert-danger">Sorry there was an error sending your message. Please try again later</div>';
            }
        }

        $data['title'] = "Contact";
		$this->load->vars($data);
		$this->load->view('header');
		$this->load->view('contact');
		$this->load->view('footer');
    }
}
